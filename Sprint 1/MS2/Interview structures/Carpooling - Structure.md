**Interview Structure**

-	Background info of interviewee
       - Name
       - Place of birth
       - Hobbies

-	Problem
       - Have you done trips inside Colombia?
       - If yes
          - To which cities?
          - Using which means?
            - Why?
              - Price?
              - Convenience?
              - Duration?
              - Comfort?
 - In the last 5 trips that you have done, which were the means of transportation that you used?
   - What was your favorite thing about each one?
   - What was something you don’t like about each one?
   - Is there a favorite?
      - Why?

- Solution
   - Have you been a user in a carpooling service
   - If yes
     - Which route did you took?
     - How did you arrange the carpooling service?
     - Did you enjoy the experience?
     - Do you agree with the price/value relationship?
     - What was your favorite part about the trip?
       - Why?
     - What was your least favorite part about the trip?
       - Why?
