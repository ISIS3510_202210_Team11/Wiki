**Interviewee:** Diana Camacho (DC)<br>
**Interviewer:** Juan Diego Martínez (JDM)<br><br>

**JDM:** Could you tell me which public services you use and pay on a regular basis?

**DC:** Water, energy, natural gas, mobile service, insurance, cooperative unions, social security, taxes, cable (TV, optic fiber internet, and telephone), school and residential fees. Usually they are paid on a monthly basis.

**JDM:** What do you think is the biggest problem or your biggest discomfort regarding the management and payment of your bills?

**DC:** There are lots of services with different prices and different payment dates, that have to be paid on different physical places or platforms. The values of some of them are even variable because they are based on consumption and not on a fixed rate, so that also increase the complexity of managing too much services at the same time. It is easy to forget the payment dates if you don't keep a close eye on them. Sometimes the providers fail to deliver the electronic bills that are sent via email so you cannot always rely on that reminder. Also, banks such as Bancolombia do not have a product suitable to SMEs (Mipymes) in order to facilitate the payment of business-related bills. For sure they have products for big enterprises, but nothing easy and cheap for a small business.

**JDM:** What is your current process for paying your bills?

**DC:** I maintain a monthly checklist of everything I need to pay and clear during the month. It is a very manual process but that has allow me to never miss a payment deadline. Some of the payments have to be made through PSE in propietary platforms of every provider and other services can be paid directly through the bank, thanks to partnerships. Before the pandemic, I used to go phisically to my bank in order to pay almost half the bills.

**JDM:** What do you think about that process? What do you like about it and what don´t you like about it?

**DC:** The process should be more simpler and faster. It is not convenient to have payments done over several platforms. The difference in dates between the bills also makes the management a little more complex.

**JDM:** Do you have a concrete idea of something you would like to do, as an individual consumer, to improve this process?

**DC:** Yes! There should be big platforms where every provider is able to put our data and where you can manage all your services from a single point. For example, being able to make a single payment instead of 20 would be excelente. They internally take charge of making the respective payments, taking a fee for their service.

**JDM:** From your personal point of view, do you think a digital tool, like an application, could be useful for these types of processes related to the management and payment of bills?

**DC:** Absolutely! An application that reunites everything, unifies dates and centralizes the payment. It also can take advantage of all the advantages of the digital world. For example, the platform should be able to connect to the already existing portals that the providers offer. It would also be interesting that the platform not only allows the user to manage its existing bills but also presents the user a catalogue of other services, showing comparative features of each one. In that way the user can also centralize the registry process of new services, choosing the best one from the catalogue.

**JDM:** If it was possible to create an application that worked as a unified public services management center, what do you think should be the most important functionalities that it should have?

**DC:** Payment center, acceptance of multiple types of payments, reminders (very important, independently that it is only 1 single payment), catalogue of services, consumption history, conditions of every service and add-ons available for each service (if applies). It would also be nice to have a rewarding system. Think of LifeMils or these kind of point-based programs. In that way the user gets benefited by discounts and the providers can receive valuable information about their users.
